package com.telegram.bot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.TelegramBotsApi;

@Component
public class TelegramXmlBotLoader implements CommandLineRunner {

    private TelegramXmlParser xmlTelegramBot;

    @Autowired
    public TelegramXmlBotLoader(TelegramXmlParser xmlTelegramBot) {
        this.xmlTelegramBot = xmlTelegramBot;
    }

    @Override
    public void run(String... args) throws Exception {
        TelegramBotsApi botsApi = new TelegramBotsApi();
        botsApi.registerBot(xmlTelegramBot);
    }
}

