package com.telegram.bot.services;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;

@Service
public class TelegramXmlParser extends TelegramLongPollingBot {

    private final static String XML_TELEGRAM_BOT_NAME = "XmlParserBot";

    private final static String XML_TELEGRAM_BOT_TOKEN = "469531219:AAHHWTxPQIFPnx1vYbgcWitoBTDEf2GVs9o";

    @Override
    public void onUpdateReceived(Update update) {
        // TODO
        if (update.getMessage().getChatId() != null && update.getMessage().hasText()) {
            sendMessageBack(prepareMessage(update.getMessage().getChatId(), update.getMessage().getText()));
        }
    }

    @Override
    public String getBotUsername() {
        return XML_TELEGRAM_BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return XML_TELEGRAM_BOT_TOKEN;
    }

    private void sendMessageBack(SendMessage sendMessage) {
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println("Error during sending message" + e);
        }
    }

    private SendMessage prepareMessage(long chatId, String messageToSend) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(messageToSend);

        return sendMessage;
    }
}